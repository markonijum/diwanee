//Gulp file 

var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var compass = require('gulp-compass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var cleanCss = require('gulp-clean-css');
var copy = require('gulp-copy');
var replace = require('gulp-replace');
var concat = require('gulp-concat');
var rename = require('gulp-rename');

var js_plugins = [
	'src/js/js_plugins/modernizr.min.js',
	'src/js/js_plugins/responsive-nav.js',
	'src/js/js_plugins/smooth-scroll.min.js',
	'src/js/js_plugins/wow.min.js'
]

var jq_plugins = [
	'src/js/jq_plugins/jquery.min.js',
	'src/js/jq_plugins/loadmore.js',
]

gulp.task('sass',function(){
	return gulp.src('src/sass/**/*.scss')
	.pipe(compass({
		config_file: './config.rb',
		css: 'src/css',
		sass: 'src/sass'
	}))
	.pipe(rename({
		suffix: ".min"
	}))
	.pipe(gulp.dest('dist/css'));
});

gulp.task('minifyCss',function(){
	return gulp.src('src/css/**/*.css')
	.pipe(sourcemaps.init())
	.pipe(cleanCss())
	.pipe(sourcemaps.write('.'))
	.pipe(rename({
		suffix: ".min"
	}))
	.pipe(gulp.dest('dist/css'));
});

gulp.task('buildCss', function(){
	return gulp.src('dist/css/**/*.css')
	.pipe(rename({
		suffix: ".min"
	}))
	.pipe(gulp.dest('dist/css'));
});

gulp.task('minifyJS', function(){
	return gulp.src(js_plugins)
	.pipe(uglify())
	.pipe(concat('js_plugins.min.js'))
	.pipe(gulp.dest('dist/js'));
});
gulp.task('minifyJQ', function(){
	return gulp.src(jq_plugins)
	.pipe(uglify())
	.pipe(concat('jq_plugins.min.js'))
	.pipe(gulp.dest('dist/js'));
});

gulp.task('mainjs', function(){
	return gulp.src('src/js/main.js')
	.pipe(sourcemaps.init())
	.pipe(uglify())
	.pipe(sourcemaps.write('.'))
	.pipe(rename('main.min.js'))
	.pipe(gulp.dest('dist/js'));
});

gulp.task('imagemin',function(){
	return gulp.src('src/img/*')
	.pipe(imagemin())
	.pipe(gulp.dest('dist/img'));
});

gulp.task('copyFonts',function(){
	return gulp.src(['src/fonts/*.*'])
	.pipe(gulp.dest('dist/fonts'));
});

gulp.task('watch', ['sass'], function(){
	gulp.watch('src/sass/**/*.scss', ['sass','minifyCss']);
	gulp.watch('src/js/main.js',['mainjs']);
})

gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./",
        // proxy: "diwanee.dev"
    });

    gulp.watch("./src/sass/**/*.scss", ['sass']);
    gulp.watch("./dist/css/*.css").on('change', browserSync.reload);
    gulp.watch("./*.html").on('change', browserSync.reload);
});


gulp.task('init',['minifyCss', 'minifyJS', 'minifyJQ', 'mainjs', 'copyFonts', 'imagemin','serve']);
gulp.task('build',['minifyCss', 'minifyJS', 'minifyJQ', 'mainjs', 'imagemin']);

